import * as types from '../types'
import router, { DynamicRoutes } from '../../router/index'
import dynamicRouter from '../../router/dynamic-router'
import { recursionRouter } from '../../utils/recursion-router'
import * as api from '../../http/api'

const state = {
  isCollapse: false, /* 导航菜单是否折叠 */
  crumbList: [], /* 面包屑导航列表 */
  permissionList: null /** 所有路由 */,
  sidebarMenu: [] /** 导航菜单 */,
  currentMenu: '' /** 当前active导航菜单 */,
  control_list: [] /** 完整的权限列表 */,
};

const mutations = {
  [types.COMMON_COLLAPSE](state, status) {
    state.isCollapse = !status
  },
  [types.COMMON_CRUMB](state, list) {
    state.crumbList = list
  },
  [types.SET_PERMISSION](state, routes) {
    state.permissionList = routes
  },
  [types.CLEAR_PERMISSION](state) {
    state.permissionList = null
  },
  [types.SET_MENU](state, menu) {
    state.sidebarMenu = menu
  },
  [types.CLEAR_MENU](state) {
    state.sidebarMenu = []
  },
  [types.SET_CURRENT_MENU](state, currentMenu) {
    state.currentMenu = currentMenu
  },
  [types.SET_CONTROL_LIST](state, list) {
    state.control_list = list
  }
};

const getters = {
};

const actions = {
  fetch_permission({ commit, state }) {
    api.fetchPermission().then(res => {
      let permissionList = res.data
      let routes = recursionRouter(permissionList.data, dynamicRouter)
      commit('SET_CONTROL_LIST', dynamicRouter)
      commit('SET_MENU', dynamicRouter)
      let initialRoutes = router.options.routes
      router.addRoutes(dynamicRouter)
      commit('SET_PERMISSION', [...initialRoutes, ...DynamicRoutes])
    })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};