// 存放所有的mutations方法名称（便于查找所有的方法名称作用是什么）
// 登录
export const LOG_GETUSERINFO = 'LOG_GETUSERINFO'; // 登录获取用户信息
export const LOGIN_IN = 'LOGIN_IN'; // 登录获取用户token
export const LOGIN_OUT = 'LOGIN_OUT'; // 登出清除token信息

// index
export const INDEX_MONEYTOTAL = 'INDEX_MONEYTOTAL';
export const INDEX_ADD = 'INDEX_ADD';

// 公共
export const COMMON_COLLAPSE = 'COMMON_COLLAPSE'; // 导航菜单是否折叠
export const COMMON_CRUMB = 'COMMON_CRUMB'; // 面包屑导航列表
export const SET_PERMISSION = 'SET_PERMISSION'; // 所有路由
export const CLEAR_PERMISSION = 'CLEAR_PERMISSION'; // 清楚所有路由
export const SET_MENU = 'SET_MENU'; // 导航菜单
export const CLEAR_MENU = 'CLEAR_MENU'; // 清楚导航菜单
export const SET_CURRENT_MENU = 'SET_CURRENT_MENU'; // 当前active导航菜单
export const SET_CONTROL_LIST = 'SET_CONTROL_LIST'; // 完整的权限列表