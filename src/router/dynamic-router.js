
import Layout from '@/views/layout/main.vue'
import BigScreen from '@/views/pages/bigScreen/bigScreen.vue'

/* 需要权限判断的路由 */
const dynamicRoutes = [
  {
    path: '',
    component: Layout,
    name: 'container',
    // redirect: 'infectiousDiseases',
    redirect: 'order',
    meta: {
      requiresAuth: false,
      name: '首页'
    },
    children:[
      {
        id: 1,
        // path: "infectiousDiseases",
        path: "/order",
        name: "order-manage",
        component: () =>import ("../views/pages/infectiousDiseases"),
        meta: {
          name: "传染病监控预警平台",
          icon: 'example'
        },
        children: [
          {
            // path: "dataCollection",
            path: "list",
            name: "order-list",
            component: () =>import ("../views/pages/infectiousDiseases/dataCollection.vue"),
            meta: {
              name: "疫情数据列表",
              requiresAuth: false,
              icon: 'table'
            }
          },
          {
            // path: "reportManagement",
            path: "product",
            name: "product-manage",
            component: () =>import ("../views/pages/infectiousDiseases/reportManagement.vue"),
            meta: {
              name: "疫情上报管理",
              requiresAuth: false,
              icon: 'user'
            }
          },
          {
            // path: "storeText",
            path: "returnGoods",
            name: "return-goods",
            component: () =>import ("../views/pages/storeText.vue"),
            meta: {
              name: "store使用样例",
              requiresAuth: false,
              icon: 'nested'
            }
          },
        ]
      },
    ],
  },
  // {
  //   path: '/bigScreen',
  //   name: "bigScreen",
  //   component: BigScreen,
  //   meta: {
  //     name: "大屏展示",
  //     requiresAuth: false
  //   }
  // },
  // {
  //   path: "/leavingMessage",
  //   name: "leavingMessage",
  //   component: () =>import ("../views/pages/smartCloud/order/leavingMessage.vue"),
  //   meta: {
  //     name: "接诊留言回复",
  //     requiresAuth: false,
  //   }
  // },
  // {
  //   path: "/videoConnection",
  //   name: "videoConnection",
  //   component: () =>import ("../views/pages/smartCloud/order/videoConnection.vue"),
  //   meta: {
  //     name: "接诊视频连线",
  //     requiresAuth: false,
  //   }
  // },
  // {
  //   path: "/viewScheduling",
  //   name: "viewScheduling",
  //   component: () =>import ("../views/pages/smartCloud/scheduling/viewScheduling.vue"),
  //   meta: {
  //     name: "查看排班整体情况",
  //     requiresAuth: false,
  //   }
  // },
  // {
  //   path: "cascaderText",
  //   name: "cascaderText",
  //   component: () =>import ("../views/pages/cascaderText.vue"),
  //   meta: {
  //     name: "cascader使用样例",
  //     requiresAuth: false,
  //   }
  // },
]

export default dynamicRoutes