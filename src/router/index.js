import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store/store.js'
import Login from '@/views/pages/login/login.vue'

Vue.use(Router)

const routes = [
  {
    path: '/login',
    name: "login",
    component: Login
  },
]

/* 准备动态添加的路由 */
export const DynamicRoutes = [
  // {
  //   path: '',
  //   component: Main,
  //   name: 'main',
  //   redirect: 'cascaderText',
  //   meta: {
  //     requiresAuth: false,
  //     name: '首页'
  //   },
  //   children:[
  //     {
  //       path: "cascaderText",
  //       name: "cascaderText",
  //       component: () =>import ("../views/pages/cascaderText.vue"),
  //       meta: {
  //         name: "cascader使用样例",
  //         requiresAuth: false,
  //       }
  //     },
  //   ],
  // },
]

const router = new Router({
  // mode: 'history',
  // base: process.env.BASE_URL,
  // 跳转路由，重新设置屏幕滚动
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0
      };
    }
  },
  routes
})


router.beforeEach((to, from, next) => {
  const token = sessionStorage.getItem("token");
  if (token) {
    if (to.matched.length > 0 && !to.matched.some(record => record.meta.requiresAuth)) {
      next()
    } else {
      next({ path: '/login' })
    }
  } else {
    if (!store.state.common.permissionList) {
      store.dispatch('common/fetch_permission').then(() => {
        next({ path: to.path })
      })
    } else {
      if (to.path !== '/login') {
        next()
      } else {
        next(from.fullPath)
      }
    }
  }
})
// // 登录验证
// router.beforeEach((to, from, next) => {
//   console.log(store.state.UserToken)
//   const token = sessionStorage.getItem("token");
//   if (to.meta.requiresAuth) {
//     if (token) {
//       next();
//     } else {
//       if (to.path == "/") {
//         next();
//       } else {
//         next("./");
//       }
//     }
//   } else {
//     next();
//   }
// });

router.afterEach((to, from, next) => {
  var routerList = to.matched
  store.commit('common/COMMON_CRUMB', routerList)
  store.commit('common/SET_CURRENT_MENU', to.name)
})

export default router;
