import $http from './http';

export const fetchPermission = () => {
  return $http.get('/api/user/info')
};
export const login = (data) =>{
  return $http.post('/api/user/login', data)
}